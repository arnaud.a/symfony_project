<?php

namespace App\Controller;

use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProjectRepository;
use App\Reposity\UserRepository;
use App\Entity\User;
use App\Entity\Project;


class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index()
    {
        $projects = $this->getDoctrine()
            ->getRepository(Project::class)
            ->getClosestProject();
        $form = $this->createForm(UserType::class);
        $userData = [];

        if ($this->getUser()){
            $i = 0;
            $userProjects=[];
            $userTeams = $this->getUser()->getUserTeams();
            foreach ($userTeams as $team) {
                $j = 0;
                $teamProject = [];
               foreach ($team->getProject() as $project){
                    $teamProject[$j] = $project;
                    $j++;
               }
               $userProjects[$i] = $teamProject;
               $i++;
            }
            $userData[0] = $userTeams;
            $userData[1] = $userProjects;
        };
        $currentUser = $this->getUser();
        return $this->render('default/index.html.twig', [
            'form' => $form->createView(),
            'controller_name' => 'DefaultController',
            'connected' => false,
            'projects' => $projects,
            'userData' => $userData,
        ]);
    }

    

}
