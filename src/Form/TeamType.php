<?php

namespace App\Form;

use App\Entity\Team;
use App\Repository\UserRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TeamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('users', EntityType::class,[
                'class' => 'App:User',
                'query_builder' => function (UserRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.name', 'ASC');
                },
                'expanded'  => false,
                'multiple'  => true,
                'choice_label' => 'name',
                'attr' => [
                    'class' => "user-team ui fluid dropdown",
                    'multiple' => ""
                ]
            ])
            ->add('manager', EntityType::class,[
                'class' => 'App:User',
                'query_builder' => function (UserRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.name', 'ASC');
                },
                'expanded'  => false,
                'multiple'  => false,
                'choice_label' => 'name',
                'attr' => [
                    'class' => "user-team ui fluid dropdown"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Team::class,
        ]);
    }
}
