<?php

namespace App\Form;

use App\Entity\Project;
use App\Repository\TeamRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('deadline', DateType::class,[
                'widget' => 'single_text'
            ])
            ->add('team', EntityType::class,[
                'class' => 'App:Team',
                'query_builder' => function (TeamRepository $er) {
                    return $er->createQueryBuilder('t')
                        ->orderBy('t.name', 'ASC');
                },
                'expanded'  => false,
                'multiple'  => true,
                'choice_label' => 'name',
                'attr' => [
                    'class' => "user-team ui fluid dropdown",
                    'multiple' => ""
                ]
            ])
            ->add('description', TextareaType::class, [
                'attr' => [
                    'class'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
        ]);
    }
}
